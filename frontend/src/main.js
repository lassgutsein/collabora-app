import { createApp } from 'vue'

import PrimeVue from 'primevue/config';

import 'primevue/resources/themes/saga-blue/theme.css';
import 'primevue/resources/primevue.min.css';
import 'primeicons/primeicons.css';
import 'pankow/resources/style.css'

import App from './App.vue'

const app = createApp(App);

app.use(PrimeVue, { ripple: true });
app.mount('#app')

FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code
WORKDIR /app/code

RUN echo "deb [signed-by=/usr/share/keyrings/collaboraonline-release-keyring.gpg] https://collaboraoffice.com/repos/CollaboraOnline/CODE-ubuntu2204 /" > /etc/apt/sources.list.d/collabora.list
RUN curl https://www.collaboraoffice.com/downloads/gpg/collaboraonline-release-keyring.gpg --output /usr/share/keyrings/collaboraonline-release-keyring.gpg

# these lines are to check for new package versions
# RUN apt-get update && apt search coolwsd && apt search code-brand && exit 1

RUN apt-get update && \
    apt-get -y install fonts-open-sans gnupg2 ca-certificates openssh-client curl hyphen-en-us hyphen-en-gb hyphen-en-gb hyphen-de hyphen-fr locales  \
    hunspell-fr hunspell-vi hunspell-uz hunspell-uk hunspell-tr hunspell-th hunspell-te hunspell-sw hunspell-sv hunspell-sr hunspell-sl hunspell-sk hunspell-si hunspell-ru hunspell-ro hunspell-pt-pt hunspell-pt-br hunspell-pl hunspell-oc hunspell-no hunspell-nl hunspell-ne hunspell-lv hunspell-lt hunspell-lo hunspell-ko hunspell-kmr hunspell-kk hunspell-it hunspell-is hunspell-id hunspell-hu hunspell-hr hunspell-hi hunspell-he hunspell-gug hunspell-gu hunspell-gl hunspell-gd hunspell-fr-classical hunspell-eu hunspell-es hunspell-en-za hunspell-en-us hunspell-en-gb hunspell-en-ca hunspell-en-au hunspell-el hunspell-dz hunspell-de-de hunspell-de-ch hunspell-de-at hunspell-da hunspell-cs hunspell-ca hunspell-bs hunspell-br hunspell-bo hunspell-bn hunspell-bg hunspell-be hunspell-ar hunspell-an hunspell-af && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# https://www.collaboraoffice.com/repos/CollaboraOnline/CODE-ubuntu2204/Packages
ARG COOLWSD_VERSION=22.05.13.1-1
ARG CODE_BRAND_VERSION=22.05-31

# collabora expects hunspell dicts (https://help.nextcloud.com/t/spelling-check-code-3-0/25178/12). NOTE: To add more languages, we have to change coolwsd.xml
# Note that adding more dictionaries requires more memory
RUN apt-get update && apt-get install -y openssl coolwsd=${COOLWSD_VERSION} code-brand=${CODE_BRAND_VERSION} && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# Generate ssl keys, those are NOT used but required for the admin panel to startup
RUN touch ~/.rnd && \
    openssl genrsa -out "/etc/coolwsd/root.key.pem" 2048 && \
    openssl req -x509 -new -nodes -key "/etc/coolwsd/root.key.pem" -days 9131 -out "/etc/coolwsd/root.crt.pem" -subj "/C=DE/ST=BW/L=Berlin/O=Dummy Authority/CN=Dummy Authority" && \
    openssl genrsa -out "/etc/coolwsd/key.pem" 2048 && \
    openssl req -key "/etc/coolwsd/key.pem" -new -sha256 -out "/etc/coolwsd/localhost.csr.pem" -subj "/C=DE/ST=BW/L=Berlin/O=Dummy Authority/CN=localhost" && \
    openssl x509 -req -in "/etc/coolwsd/localhost.csr.pem" -CA "/etc/coolwsd/root.crt.pem" -CAkey "/etc/coolwsd/root.key.pem" -CAcreateserial -out "/etc/coolwsd/cert.pem" -days 9131 && \
    mv /etc/coolwsd/root.crt.pem /etc/coolwsd/ca-chain.cert.pem

RUN ln -s /app/data/fonts /opt/collaboraoffice/share/fonts/truetype/local

RUN cp /etc/coolwsd/coolwsd.xml /etc/coolwsd/coolwsd.xml.save
RUN rm -rf /etc/coolwsd/coolwsd.xml && ln -s /app/data/coolwsd.xml /etc/coolwsd/coolwsd.xml

RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/ /etc/supervisor/conf.d/

COPY package.json package-lock.json /app/code/
RUN npm install --production

COPY start.sh nginx.conf coolwsd.xml server.js /app/code/

COPY frontend /tmp/frontend
RUN cd /tmp/frontend/ && \
    npm i && \
    npm run build && \
    mv /tmp/frontend/dist /app/code/frontend && \
    rm -rf /tmp/frontend

# generate the en_US.UTF-8 locale for collabora to open files with special chars
RUN sed -e 's,^# en_US.UTF-8 UTF-8,en_US.UTF-8 UTF-8,' -i /etc/locale.gen && \
    locale-gen

CMD [ "/app/code/start.sh" ]

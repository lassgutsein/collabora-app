#!/usr/bin/env node

'use strict';

var exec = require('child_process').exec,
    express = require('express'),
    fs = require('fs'),
    morgan = require('morgan'),
    path = require('path'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    lastMile = require('connect-lastmile'),
    ldap = require('ldapjs'),
    HttpError = require('connect-lastmile').HttpError,
    HttpSuccess = require('connect-lastmile').HttpSuccess,
    xmlButPrettier = require('xml-but-prettier'),
    xml2json = require('xml2json');

const ldapClient = ldap.createClient({
    url: process.env.CLOUDRON_LDAP_URL,
    timeout: 10000, /* 10 seconds */
    reconnect: true /* undocumented option to automatically reconnect on connection failure : https://github.com/joyent/node-ldapjs/issues/318#issuecomment-165769581  */
});

const COOL_CONFIG_FILE = process.env.CLOUDRON ? '/app/data/coolwsd.xml' : path.resolve(__dirname, 'coolwsd_test.xml');

function isAuthenticated(req, res, next) {
    if (req.session && req.session.user) return next();

    res.status(401).send({});
}

function login(req, res, next) {
    if (!req.body.username || !req.body.password) {
        req.session.user = null;
        return next(new HttpError(401, 'Unauthorized'));
    }

    ldapClient.bind(process.env.CLOUDRON_LDAP_BIND_DN, process.env.CLOUDRON_LDAP_BIND_PASSWORD, function (error) {
        if (error) return next(new HttpError(500, error));

        ldapClient.search(process.env.CLOUDRON_LDAP_USERS_BASE_DN, { filter: `(|(uid=${req.body.username})(mail=${req.body.username})(username=${req.body.username})(sAMAccountName=${req.body.username}))` }, function (error, result) {
            if (error) return next(new HttpError(500, error));

            var items = [];

            result.on('searchEntry', function(entry) {
                items.push(entry.object);
            });

            result.on('error', function (error) {
                next(new HttpError(500, error));
            });

            result.on('end', function (result) {
                if (result.status !== 0) return next(new HttpError(500, error));
                if (items.length === 0) return next(new HttpError(401, 'Unauthorized'));

                ldapClient.bind(items[0].dn, req.body.password, function (error) {
                    if (error && error.code === ldap.LDAP_INVALID_CREDENTIALS) return next(new HttpError(401, 'Unauthorized'));
                    if (error) return next(new HttpError(500, error));

                    // translate propertry names
                    items[0].id = items[0].uid;

                    req.session.user = {
                        id: items[0].uid,
                        username: items[0].username,
                        displayName: items[0].displayname,
                        email: items[0].mail
                    };

                    next(new HttpSuccess(200, req.session.user));
                });
            });
        });
    });
}

function logout(req, res) {
    req.session.user = null
    res.redirect('/')
}

function getProfile(req, res, next) {
    next(new HttpSuccess(200, { user: req.session.user }))
}

function healthcheck(req, res, next) {
    next(new HttpSuccess(200, {}));
}

function getSettings(req, res, next) {
    fs.readFile(COOL_CONFIG_FILE, function (error, result) {
        if (error) return next(new HttpError(500, error));

        var settings = xml2json.toJson(result, { object: true, reversible: true });
        var wopi = settings.config.storage.wopi;

        // Only one wopi host is supported at all times
        if (Array.isArray(wopi.host)) wopi.host = wopi.host[0];

        next(new HttpSuccess(200, {
            allowedHost: wopi.host['$t'],
            adminConsole: {
                username: settings.config.admin_console.username,
                password: settings.config.admin_console.password
            }
        }));
    });
}

function setSettings(req, res, next) {
    fs.readFile(COOL_CONFIG_FILE, function (error, result) {
        if (error) return next(new HttpError(500, error));

        var settings = xml2json.toJson(result, { object: true });

        if (!settings.config.storage) settings.config.storage = {};
        if (!settings.config.storage.wopi) settings.config.storage.wopi = { desc: 'Allow/deny wopi storage. Mutually exclusive with webdav.', allow: 'true' };

        settings.config.storage.wopi.host = {
            desc: 'Regex pattern of hostname to allow or deny.',
            allow: 'true',
            '$t': req.body.allowedHost.trim()
        };

        fs.writeFile(COOL_CONFIG_FILE, xmlButPrettier(xml2json.toXml(JSON.stringify(settings)), { textNodesOnSameLine: true }), function (error) {
            if (error) return next(new HttpError(500, error));

            if (process.env.CLOUDRON) {
                console.log('Restarting cool process');
                exec('supervisorctl restart cool');
            }

            next(new HttpSuccess(201, {}));
        });
    });
}

// Setup the express server and routes
var app = express();
var router = new express.Router();

router.post  ('/api/login', login);
router.post  ('/api/logout', logout);
router.get   ('/api/settings', isAuthenticated, getSettings);
router.post  ('/api/settings', isAuthenticated, setSettings);
router.get   ('/api/profile', isAuthenticated, getProfile);
router.get   ('/api/healthcheck', healthcheck);

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false, limit: '100mb' }));
app.use(cookieParser());
app.use(session({ secret: 'office collab', resave: false, saveUninitialized: false }));
app.use(router);
app.use('/', express.static(__dirname + '/frontend'));
app.use(lastMile());

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    console.log('Collabora admin listening on http://%s:%s', host, port);
});

[1.0.0]
* Initial version

[1.0.1]
* Improved settings panel

[1.1.0]
* Use new Cloudron base image

[1.2.0]
* Add dictionaries for spell check

[1.3.0]
* Update to version 4

[1.4.0]
* Support Mobile Editing

[1.5.0]
* Update loolwsd to 4.0.3
* Update code to 4.0-2

[1.6.0]
* Update loolwsd to 4.0.4-3
* Use manifest v2

[1.7.0]
* Update Collabora to 4.2.0

[1.7.1]
* Update Collabora to 4.2.1

[1.7.2]
* Update Collabora to 4.2.2

[1.7.3]
* Update Collabora to 4.2.3

[1.8.0]
* Use latest base image 2.0.0
* Update Collabora to 4.2.3-2

[1.8.1]
* Update Collabora to 4.2.4-4

[1.8.2]
* Update Collabora to 4.2.5

[1.8.3]
* Update Collabora to 4.2.5-6

[1.8.4]
* Update Collabora to 4.2.6-2

[1.9.0]
* Update Collabora to 6.4.0

[1.9.1]
* Update Collabora to 6.4.0-14

[1.9.2]
* Update Collabora to 6.4.0-15
* Enable new toolbar style (requires reinstall of the app)

[1.9.3]
* Update Collabora to 6.4.1-4

[1.9.4]
* Update Collabora to 6.4.2-2

[1.9.5]
* Update loolwsd to 6.4.2-3
* Update code to 6.4-9

[1.9.6]
* Update loolwsd to 6.4.3-2
* Update CODE to 6.4-11

[1.9.7]
* Update to 6.4.4-3

[1.9.8]
* Update to 6.4.5-3

[1.10.0]
* Use base image version 3

[1.10.1]
* Update to 6.4.6-2

[1.10.2]
* Update to 6.4.6-6

[1.10.3]
* Update loolwsd to 6.4.7-2
* Update code to 6.4-12

[1.10.4]
* Update loolwsd to 6.4.7-5

[1.10.5]
* Update loolwsd to 6.4.7-6

[1.10.6]
* Update loolwsd to 6.4.8-1

[1.10.7]
* Update loolwsd to 6.4.8-3
* Update code to 6.4-14

[1.10.8]
* Update loolwsd to 6.4.8-4

[1.10.9]
* Update loolwsd to 6.4.8-6
* Update code to 6.4-15

[1.10.10]
* Update loolwsd to 6.4.9-1
* Update code to 6.4-16

[1.10.11]
* Update loolwsd to 6.4.9-3

[1.10.12]
* Update loolwsd to 6.4.10-2
* Update code to 6.4-19

[1.10.13]
* Update loolwsd to 6.4.10-5

[1.10.14]
* Update loolwsd to 6.4.10-9

[1.10.15]
* Update loolwsd to 6.4.10-10

[1.10.16]
* Update loolwsd to 6.4.13-1
* Update code to 6.4-22

[1.10.17]
* Update loolwsd to 6.4.13-2

[1.10.18]
* Update loolwsd to 6.4.13-3
* Update code to 6.4-23

[1.10.19]
* Update package meta information
* Update loolwsd to 6.4.14-2

[1.10.20]
* Update loolwsd to 6.4.14-3

[1.11.0]
* Update to v21

[1.11.1]
* Update coolwsd to 21.11.0.6-1
* Update CODE to 21.11-12

[1.11.2]
* Update coolwsd to 21.11.1.0-1

[1.11.3]
* Update coolwsd to 21.11.1.1-1
* Update CODE to 21.11-13
* Update base image to 3.2.0

[1.11.4]
* Update coolwsd to 21.11.1.3-1
* Update CODE to 21.11-14

[1.11.5]
* Update coolwsd to 21.11.1.4-1

[1.11.6]
* Update coolwsd to 21.11.2.2-1
* Update CODE to 21.11-15

[1.11.7]
* Update coolwsd to 21.11.2.3-1
* Update CODE to 21.11-16

[1.11.8]
* Update coolwsd to 21.11.2.4-1
* Update CODE to 21.11-17

[1.11.9]
* Update coolwsd to 21.11.3.4-1
* Update CODE to 21.11-20

[1.11.10]
* Update coolwsd to 21.11.3.6-1
* Update CODE to 21.11-22

[1.11.11]
* Update coolwsd to 21.11.4.1-1
* Update CODE to 21.11-23

[1.11.12]
* Update coolwsd to 21.11.4.2-1
* Update CODE to 21.11-26

[1.11.13]
* Update coolwsd to 21.11.4.3-1
* Update CODE to 21.11-27

[1.11.14]
* Update coolwsd to 21.11.5.1-1

[1.11.15]
* Update coolwsd to 21.11.5.3-1

[1.12.0]
* Update coolwsd to 22.05.3.1-1
* Update CODE to 22.05-9
* Install all hunspell directories
* Increase memory limit to 1Gb

[1.12.1]
* Update coolwsd to 22.05.4.1-1
* Update CODE to 22.05-10

[1.12.2]
* Update coolwsd to 22.05.5.1
* Update CODE to 22.05-11

[1.12.3]
* Update coolwsd to 22.05.5.2
* Update CODE to 22.05-13

[1.12.4]
* Update coolwsd to 22.05.5.3
* Update CODE to 22.05-14

[1.12.5]
* Add support for custom TTF fonts

[1.12.6]
* Update coolwsd to 22.05.5.4

[1.12.7]
* Update coolwsd to 22.05.6.1
* Update CODE to 22.05-15

[1.12.8]
* Update coolwsd to 22.05.6.3

[1.12.9]
* Update coolwsd to 22.05.7.2
* Update CODE to 22.05-16

[1.12.10]
* Update coolwsd to 22.05.7.3
* Update CODE to 22.05-17

[1.13.0]
* Update coolwsd to 22.06.8.1
* Update CODE to 22.05-18

[1.13.1]
* Update coolwsd to 22.06.8.2

[1.13.2]
* Update coolwsd to 22.06.8.4
* Update CODE to 22.05-19

[1.13.3]
* Update coolwsd to 22.05.9.2
* Update CODE to 22.05-21

[1.13.4]
* Update Cloudron base image to 4.0.0

[1.13.5]
* Update coolwsd to 22.05.9.3
* Update CODE to 22.05-23

[1.13.6]
* Update coolwsd to 22.05.10.1
* Update CODE to 22.05-24

[1.13.7]
* Update coolwsd to 22.05.10.2
* Update CODE to 22.05-26

[1.13.8]
* Update coolwsd to 22.05.10.8

[1.13.9]
* Update settings UI

[1.13.10]
* Update coolwsd to 22.05.12.1
* Update CODE to 22.05-28

[1.13.11]
* Update coolwsd to 22.05.12.2

[1.13.12]
* Update coolwsd to 22.05.12.3

[1.13.13]
* Update coolwsd to 22.05.13.1
* Update CODE to 22.05-31


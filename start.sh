#!/bin/bash

set -eu

echo "=> Ensure directories"
mkdir -p /run/child-roots /run/coolwsdcache /app/data/fonts

echo "=> Copy runtime files for jails to the same /run filesystem"
cp -rf /opt/cool/systemplate /run/systemplate
cp -rf /opt/collaboraoffice /run/collaboraoffice

echo "=> Fix up permissions to cool:cool"
chown cool:cool -R /run

if [[ -f "/app/data/loolwsd.xml" ]]; then
    echo "=> Migrating from old loolwsd naming scheme"
    mv /app/data/loolwsd.xml /app/data/coolwsd.xml
    sed "s/loolwsd/coolwsd/g" -i /app/data/coolwsd.xml
fi

echo "=> Ensure main config file coolwsd.xml"
if [[ ! -f "/app/data/coolwsd.xml" ]]; then
    cp /app/code/coolwsd.xml /app/data/coolwsd.xml

    # Generate a unique admin password
    ADMIN_PASSWORD=$(pwgen -s 8 1)
    perl -pi -e "s/<password (.*)>.*<\/password>/<password \1>${ADMIN_PASSWORD}<\/password>/" /app/data/coolwsd.xml

    # Pre-provision with whitelisting apps on this domain, as being a common use-case
    WHITELIST_DOMAIN=$(echo "${APP_DOMAIN}" | sed 's/^[^\.]*\.//g')
    perl -pi -e "s/<host (.*)>.*<\/host>/<host \1>[a-zA-Z0-9_\\\-.]*${WHITELIST_DOMAIN}<\/host>/" /app/data/coolwsd.xml
fi

echo "=> Make the config file writeable by cloudron"
chown cloudron /app/data/coolwsd.xml

echo "=> Start supervisor"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i collabora

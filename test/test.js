#!/usr/bin/env node

/* jslint node:true */
/* global it:false */
/* global xit:false */
/* global describe:false */
/* global before:false */
/* global after:false */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;

    const DEFAULT_PATTERN = '[a-zA-Z0-9_\\-.]*';
    let pattern = DEFAULT_PATTERN;

    let browser, app;

    before(function (done) {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
        done();
    });

    after(function () {
        browser.quit();
    });

    async function exists(selector) {
        await browser.wait(until.elementLocated(selector), TEST_TIMEOUT);
    }

    async function visible(selector) {
        await exists(selector);
        await browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
    }

    async function login() {
        await browser.get(`https://${app.fqdn}`);
        await visible(By.id('usernameInput'));
        await browser.findElement(By.id('usernameInput')).sendKeys(username);
        await browser.findElement(By.id('passwordInput')).sendKeys(password);
        await browser.findElement(By.id('loginButton')).click();
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Settings")]')), TEST_TIMEOUT);
    }

    async function checkSettings() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Settings")]')), TEST_TIMEOUT);
        const value = await browser.findElement(By.xpath('//form//input')).getAttribute('value');
        expect(value).to.equal(pattern);
    }

    async function saveSettings() {
        pattern = DEFAULT_PATTERN + 'example.com';

        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(), "Settings")]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//form//input')).sendKeys('example.com');
        await browser.findElement(By.xpath('//span[text()="Save"]')).click();
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Logout"]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//span[text()="Logout"]')).click();
        await visible(By.id('usernameInput'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('can login', login);
    it('can see settings', checkSettings);
    it('can save settings', saveSettings);
    it('can see saved settings', checkSettings);
    it('can logout', logout);

    it('restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('can see settings', checkSettings);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can see settings', checkSettings);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can see settings', checkSettings);

    it('uninstall app', function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync(`cloudron install --appstore-id ${app.manifest.id} --location ${LOCATION}`, EXEC_ARGS);
        pattern = DEFAULT_PATTERN;
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can save settings', saveSettings);

    it('can update', function () {
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });

    it('can get settings', checkSettings);

    it('uninstall app', function () {
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});

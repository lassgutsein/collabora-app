Collabora Online is an office suite for NextCloud.

### Overview

Collabora Online is a powerful LibreOffice-based online office that supports all major document, spreadsheet and presentation file formats, which you can integrate in your own infrastructure.

It allows collaborative editing with excellent office file format support.

### Key features

- View and edit text documents, spreadsheets, presentations & more
- Collaborative editing features
- Long Term Support and signed security updates
- Works in any modern browser – no plugin needed

### Document support

- Preservation of layout and formatting of documents
- text documents (odt, docx, doc,…)
- spreadsheets (ods, xlsx, xls, …)
- presentations (odp, pptx, ppt,…)
